# flow3r imports
import random
import bl00mbox
import time
import math
import leds
from st3m import InputState, Responder
from st3m.application import Application, ApplicationContext
from st3m.ui.colours import BLUE, WHITE
from st3m.goose import Optional
from st3m.utils import xy_from_polar, tau
from st3m.ui.view import ViewManager
from st3m.ui.interactions import CapScrollController
from ctx import Context


class Blob(Responder):
    def __init__(self) -> None:
        self._yell = 0.0
        self._blink = False
        self._blinking: Optional[int] = None

    def think(self, ins: InputState, delta_ms: int) -> None:
        if self._blinking is None:
            if random.random() > 0.99:
                self._blinking = 100
        else:
            self._blinking -= delta_ms
            if self._blinking < 0:
                self._blinking = None

    def draw(self, ctx: Context) -> None:
        blink = self._blinking is not None
        v = self._yell
        if v > 1.0:
            v = 1.0
        if v < 0:
            v = 0

        v /= 1.5
        if v < 0.1:
            v = 0.1

        ctx.rgb(62 / 255, 159 / 255, 229 / 255)

        ctx.save()
        ctx.rotate(-v)
        ctx.arc(0, 0, 80, tau, tau / 2, 1)
        ctx.fill()

        ctx.gray(60 / 255)
        if blink:
            ctx.line_width = 1
            ctx.move_to(50, -30)
            ctx.line_to(70, -30)
            ctx.stroke()
            ctx.move_to(30, -20)
            ctx.line_to(50, -20)
            ctx.stroke()
        else:
            ctx.arc(60, -30, 10, 0, tau, 0)
            ctx.arc(40, -20, 10, 0, tau, 0)
            ctx.fill()
        ctx.restore()

        ctx.arc(0, 0, 80, v / 2, tau / 2 + v / 2, 0)
        ctx.fill()

        ctx.rectangle(-80, 0, 20, -120)
        ctx.fill()


class IMFallin(Application):
    PETAL_NO = 7

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._blob = Blob()
        self._acc = 0
        self._last_accs = [0 for _ in range(10)]
        self._last_sums = [0 for _ in range(30)]

        self._blm = bl00mbox.Channel("Otamatone")

        # Sawtooth oscillator
        self._osc = self._blm.new(bl00mbox.patches.tinysynth)
        # Distortion plugin used as a LUT to convert sawtooth into custom square
        # wave.
        self._dist = self._blm.new(bl00mbox.plugins._distortion)
        # Lowpass.
        self._lp = self._blm.new(bl00mbox.plugins.lowpass)

        # Wire sawtooth -> distortion -> lowpass
        self._osc.signals.output = self._dist.signals.input
        self._dist.signals.output = self._lp.signals.input
        self._lp.signals.output = self._blm.mixer

        # Closest thing to a waveform number for 'saw'.
        self._osc.signals.waveform = 20000

        # Built custom square wave (low duty cycle)
        table_len = 129
        self._dist.table = [
            32767 if i > (0.1 * table_len) else -32768 for i in range(table_len)
        ]

        self._lp.signals.freq = 4000

        self._intensity = 0.0

        self.input.captouch.petals[self.PETAL_NO].whole.repeat_disable()
        self._scale_name = 1.0
        self._led = 0.0
        self._phase = 0.0


    def draw(self, ctx: Context) -> None:
        ctx.rectangle(-120, -120, 240, 240)
        ctx.rgb(0, 0, 0)
        ctx.fill()

        self._blob.draw(ctx)


    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self._blob.think(ins, delta_ms)
        self._acc = ins.imu.acc[0]**2 + ins.imu.acc[1]**2 + ins.imu.acc[2]**2
        self._last_accs.append(self._acc)
        self._last_accs = self._last_accs[1:]
        self._last_sums.append(sum(self._last_accs))
        self._last_sums = self._last_sums[1:]

        max_last_sum = max(self._last_sums)
        self._blob._yell = max_last_sum / 3000
        
        self._phase += delta_ms / 1000
        self._scale_name = math.sin(self._phase)
        
        if self._blob._yell > 0.7:
            for _ in range(min(int(self._blob._yell/3), 5)):
                self._last_sums.append(max_last_sum)
            self._osc.signals.pitch.tone = self._blob._yell * 12
            self._intensity  = self._blob._yell
            self._osc.signals.trigger.start()

            for led in range(40):
                leds.set_hsv(int(led), 0, 1, 1)
            leds.update()
        else:
            self._intensity = 0
            self._osc.signals.trigger.stop()
            self._last_sums = self._last_sums[:30]
            self._led += delta_ms / 45
            if self._led >= 40:
                self._led = 0
                
            leds.set_hsv(int(self._led), abs(self._scale_name) * 360, 1, 0.2)
            leds.update()

# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(IMFallin(ApplicationContext()))

